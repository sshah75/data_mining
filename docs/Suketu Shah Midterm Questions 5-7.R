#Suketu Shah
#CS513 - Midterm
#CWID - 10355595

#delete all the environment variables
rm(list=ls())

#including the libraries for histograms, boxplot & knn
library(class)
library(ggplot2)

#Question 5

    #5.1 and 5.2 have been performed as requested.
    
    #5.3 Reading the .csv file
    	infile <- read.csv("C:/Junk/churn.csv")
    	attach(infile)
    
    #5.4
    
		#summary of both Night_Mins and Day_Mins
		summary(Night_Mins)
		summary(Day_Mins)
    
    	# calculating the minimum of Night_Mins and Day_Mins
    	night_mins_min <- min(Night_Mins, na.rm = TRUE)
		day_mins_min <- min(Day_Mins, na.rm = TRUE)
		print(night_mins_min)
		print(day_mins_min)
    	
    	# calculating the maximum of Night_Mins and Day_Mins
    	night_mins_max <- max(Night_Mins, na.rm = TRUE)
    	day_mins_max <- max(Day_Mins, na.rm = TRUE)
		print(night_mins_max)
		print(day_mins_max)
    
    	# calculating the median of Night_Mins and Day_Mins
    	night_mins_median <- median(Night_Mins, na.rm = TRUE)
    	day_mins_median <- median(Day_Mins, na.rm = TRUE)
    	print(night_mins_median)
		print(day_mins_median)
    	
		# calculating the first quartile of Night_Mins and Day_Mins
    	c <- quantile(Day_Mins)
    	cat("The first quartile for Day_Mins is: ", c[2], "\n")
    	c <- quantile(Night_Mins)
    	cat("The first quartile for Night_Mins is: ", c[2], "\n")
    	
    	# drawing the boxplot of Night_Mins and Day_Mins
    	day_mins_boxplot <- boxplot(Day_Mins)
    	night_mins_boxplot <- boxplot(Night_Mins)
    	
		# listing the outliers of Night_Mins and Day_Mins
		day_mins_boxplot$out
		night_mins_boxplot$out

		# drawing the histograms of Night_Mins and Day_Mins
    	hist(Night_Mins)
    	hist(Day_Mins)
    
		#missing outliers
          
    #5.5 2-way Frequency Table
    	frequency_table <- ftable(data.frame(Int_l_Plan, VMail_Plan))
    	View(frequency_table)
    
    	# Another alternative to create the frequency table is just to use the table function call 
    	# table(Int_l_Plan, VMail_Plan)



#Question 6

  #Question 6.1

    #function to calculate the probability of a customer having both an international and voice plan
    intlAndVoicePlan <- function(vector1, vector2)
    {
      count <- 0
      
      for(i in seq(1, length(vector1)))
      {
        if(vector1[i] == vector2[i] && vector1[i] == "yes")
        {
          count <- count + 1
        }
      }
      
      return (count/length(vector1))
    }

    #function call which calculates the probability of a customer having both an international and voice plan
    intl_and_vmail_Plans <- intlAndVoicePlan(Int_l_Plan, VMail_Plan)

    #print the number in console
    print(intl_and_vmail_Plans)

  
  
  #Question 6.2

    #function to calculate the probability if a random person is a churner
    isChurner <- function(vector1)
    {
      count <- 0
      
      for(i in seq(1, length(vector1)))
      {
        if(vector1[i] == "True.")
        {
          count <- count + 1
        }
      }
      
      return (count/length(vector1))
    }

    #function call which will calculate the probability
    is_churner_probability <- isChurner(churn)
    print(is_churner_probability)

  
  
  #Question 6.3

    #function to calculate the probability if a random person is a churner and has an international plan
    intlAndChurner <- function(vector1, vector2)
    {
      count <- 0
      
      for(i in seq(1, length(vector1)))
      {
        if(vector1[i] == "yes" && vector2[i] == "True.")
        {
          count <- count + 1
        }
      }
      
      return (count/length(vector1))
    }

    #function call which will calculate the probability if a random person is a churner and has an international plan
    intersectionProbability <- intlAndChurner(Int_l_Plan, churn)
    #print(intersectionProbability)
    probabilityChurner <- isChurner(churn)
    #print(probabilityChurner)
    probabilityIntlAndChurn <- intersectionProbability/probabilityChurner
    print(probabilityIntlAndChurn)

  
  
  #Question 6.4

    #function call which will calculate the probability if a random person is a churner and has an voicemail plan
    intersectionProbability <- intlAndChurner(VMail_Plan, churn)
    #print(intersectionProbability)
    probabilityChurner <- isChurner(churn)
    #print(probabilityChurner)
    probabilityVMailAndChurn <- intersectionProbability/probabilityChurner
    print(probabilityVMailAndChurn)

  
  
  #Question 6.5

    # function to calculate the probability of international plans
    intlPlan <- function(vector1)
    {
      count <- 0
      
      for(i in seq(1, length(vector1)))
      {
        if(vector1[i] == "yes")
        {
          count <- count + 1
        }
      }
      
      return (count/length(vector1))
    }

    # function to determine if churning and international plan are independent events
    independenceCheck <- function(vector1, vector2)
    {
      sum <- 0
      
      for(i in seq(1, length(vector1)))
      {
        if(vector1[i] == "yes" && vector2[i] == "True.")
        {
          sum <- sum + 1
        }
      }
      
      intersectionProbability <- (sum/length(vector1))
      cat("Probability of International Plan & Churn is: ", intersectionProbability, "\n")
      intlProbability <- intlPlan(vector1)
      cat("Probability of International Plan is: ", intlProbability, "\n")
      churnProbability <- isChurner(vector2)
      cat("Probability if a customer is a churner: ", churnProbability, "\n")
      
      if(intersectionProbability == intlProbability*churnProbability)
      {
        cat("Churning and having international plan are independent events!", "\n")
      }
      
      else
      {
        cat("Churning and having international plan are NOT independent (dependent) events!", "\n")
      }
    }

    # function call to determine if churning and international plan are independent events
    independenceCheck(Int_l_Plan, churn)


	
	
#Question 7

      #creating a subset dataset of the only required columns
      mydata <- data.frame(Intl_Plan = Int_l_Plan, VMail_Plan = VMail_Plan, Day_Mins = Day_Mins, Eve_Mins = Eve_Mins, Night_Mins = Night_Mins, churn = churn)
      #View(mydata)
      
      #create the sequence with every fifth row of the data set
      idx <- seq(1, nrow(mydata), 5)
      
      #create the test dataset
      test <- mydata[idx,]
      
      #create the training dataset
      training <- mydata[-idx,]
      
      #function to normalize the dataset
      normalize <- function(dataset)
      {
        minVal <- min(dataset)
        maxVal <- max(dataset)
        num <- maxVal - minVal
        
        for(i in seq(1,length(dataset)))
        {
          dataset[i] <- ((dataset[i] - minVal) / num)
        }
        
        return(dataset)
      }
      
      #function to compare 2 vectors and return the number of true & false
      compare <- function(vector1, vector2)
      {
        true <- 0
        false <- 0
        
        for(i in seq(1,length(vector1)))
        {
          res <- (vector1[i] == vector2[i])
          
          cat(res,"\n")
          
          if(res == TRUE)
            true <- true + 1
          else
            false <- false + 1
        }
        
        return(c(true,false))
      }
      
      #normalize both the datasets
      nTraining <- training
      nTest <- test
      nTraining[,1] <- ifelse(nTraining[,1]=='yes',1,0)
      nTraining[,2] <- ifelse(nTraining[,2]=='yes',1,0)
      nTraining[,3] <- normalize(training[,3])
      nTraining[,4] <- normalize(training[,4])
      nTraining[,5] <- normalize(training[,5])
      nTraining[,6] <- ifelse(nTraining[,6]=='True.',1,0)
      nTest[,1] <- ifelse(nTest[,1]=='yes',1,0)
      nTest[,2] <- ifelse(nTest[,2]=='yes',1,0)
      nTest[,3] <- normalize(test[,3])
      nTest[,4] <- normalize(test[,4])
      nTest[,5] <- normalize(test[,5])
      nTest[,6] <- ifelse(nTest[,6]=='True.',1,0)

      #perform the knn method
      k1 <- knn(nTraining[,-6], nTest[,-6], nTraining[,6], k = 1)
      k3 <- knn(nTraining[,-6], nTest[,-6], nTraining[,6], k = 3)
      k5 <- knn(nTraining[,-6], nTest[,-6], nTraining[,6], k = 5)
      
      #function to find out and tell us the best k value out of the 3 and print the error statistics
      bestK <- function()
      {
        res1 <- compare(k1, nTest[,6])
        max <- res1[1]
        kval <- 1
        
        res3 <- compare(k3, nTest[,6])
        if(max < res3[1])
        {
          max <- res3[1]
          kval <- 3
        }
        
        res5 <- compare(k5, nTest[,6])
        if(max < res5[1])
        {
          max <- res5[1]
          kval <- 5
        }
        
        cat("The number of correct responses for k value = 1 were ", res1[1], " out of ", res1[1]+res1[2], "which is an error rate of ", (res1[2]/(res1[1]+res1[2]))*100, "%", "\n")
        cat("The number of correct responses for k value = 3 were ", res3[1], " out of ", res3[1]+res3[2], "which is an error rate of ", (res3[2]/(res3[1]+res3[2]))*100, "%", "\n")
        cat("The number of correct responses for k value = 5 were ", res5[1], " out of ", res5[1]+res5[2], "which is an error rate of ", (res5[2]/(res5[1]+res5[2]))*100, "%", "\n")
        cat("The best output was for k value = ", kval, "\n")
      }
      
      # function to find out which is the best k value out of the 3
      bestK()

#detach the input file
detach(infile)

#delete all the environment variables
rm(list=ls())